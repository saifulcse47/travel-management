<?php

namespace App\Http\Controllers;

use App\AddPackage;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AddPackageController extends Controller
{
    public function addPackage(){
        return view('admin.addpackage.addpackage');
    }

    public function newAddPackage(Request $request){
        $add_package = new AddPackage();
        $image = $request->file('package_image');
        $imageName = $image->getClientOriginalName();
        $directory = 'package-images/';
        $image->move($directory, $imageName);

        $add_package->package_name          = $request->package_name;
        $add_package->package_description   = $request->package_description;
        $add_package->package_price         = $request->package_price;
        $add_package->package_image         = $directory.$imageName;
        $add_package->save();

        return redirect('/add-package')->with('message', 'Package Add Successfully');
    }

    public function viewPackage(){
        return view('admin.addpackage.view-package',[
            'packages' => AddPackage::orderBy('id','desc')->get()
        ]);
    }

    public function editPackage($id){
        return view('admin.addpackage.edit-package',[
            'package' => AddPackage::find($id)
        ]);
    }

    public function updatePackage(Request $request){

        $request->validate([
            'package_name' => 'required',
            'package_description' => 'required',
            'package_price' => 'required',
            'package_image' => 'required',
        ]);

        $previousPackage = AddPackage::find($request->package_id);

        $image = $request->file('package_image');
        $imageName = $image->getClientOriginalName();
        $directory = 'package-images/';
        $image->move($directory, $imageName);
        $url = $directory.$imageName;


        $previousPackage->package_name = $request->package_name;
        $previousPackage->package_description = $request->package_description;
        $previousPackage->package_price = $request->package_price;
        $previousPackage->package_image= $url;
        $previousPackage->save();

        return view('admin.addpackage.view-package',[
            'packages' => AddPackage::orderBy('id','desc')->get()
        ]);
    }

    public function deletePackage($id){
        AddPackage::find($id)->delete();
        return redirect('/view-package')->with('message', 'Add Package info delete successfully');
    }

    public function bookForm($id){
        return view('front.package.book-package',[
            'package' => AddPackage::find($id)
        ]);
    }

    public function bookTable($customerId){
//       return DB::table('customers')
//           ->join('book_packages','customers.id','=','book_packages.customer_id')
//           ->where('customers.id',$customerId)->get();
//        return view('front.package.add-package-table',['bookingInfos'=>$bookingInfos]);
    }

    public function customerBookPackageTable($id){
     $customerInfos = DB::table('customers')
            ->join('book_packages','customers.id','=','book_packages.customer_id')
            ->where('customers.id',$id)
            ->select('customers.*','book_packages.*')
         ->orderBy('customers.id','desc')
            ->get();
        return view('front.package.book-package-table',['customerInfos'=>$customerInfos]);
    }

    public function bookNow(){
        return view('front.package.book-package');
    }


}
