<?php

namespace App\Http\Controllers;

use App\AddPackage;
use App\BookPackage;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BookPackageController extends Controller
{
    public function BookPackage(Request $request){
        //return $request->all();
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'contact_num' => 'required',
            'booking_date' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'nid' => 'required',
            'passport_no' => 'required',
        ]);
        // BookPackage::saveBookPackageInfo($request);

        $package = AddPackage::find($request->select_package);
        $customer = new Customer();
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->contact_num = $request->contact_num;
        $customer->email = $request->email;
        $customer->password = bcrypt($request->password);
        $customer->gender = $request->gender;
        $customer->nid = $request->nid;
        $customer->passport_no = $request->passport_no;
        $customer->save();

        $bookPackage = new BookPackage();
        $bookPackage->customer_id = $customer->id;
        $bookPackage->booking_date = $request->booking_date;
        $bookPackage->select_package = $package->package_name;
        $bookPackage->save();

        return redirect('/book-package-customer-table/'.$customer->id)->with('message','book package successfully');
    }

    public function bookTable()
    {
        return view('admin.bookingpackage.booking-list',[
            'bookpackages' => BookPackage::all()
        ]);
    }

    public function PackageConfirmation()
    {
        return view('admin.bookingpackage.booking-list',[
            'bookpackages' => BookPackage::all()
        ]);
    }


}
