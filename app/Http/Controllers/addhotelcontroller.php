<?php

namespace App\Http\Controllers;

use App\addhotel;
use App\CustomerHotelBooking;
use Illuminate\Http\Request;

class addhotelcontroller extends Controller
{
    public function addhotel(){
        return view('admin.addhotel.addhotel');
    }

    public function newAddHotel(Request $request)
    {
        AddHotel::saveAddHotelInfo($request);

        return redirect('/addhotel')->with('message', 'Hotel Add Successfully');
    }

    public function viewHotel()
    {
        return view('front.hotel.hotel', [
            'hotels' => AddHotel::all()
        ]);
    }

    public function hotelBookingForm($id)
    {
        return view('front.hotel.hotel-booking-form', [
            'hotel' => AddHotel::find($id)
        ]);
    }

    public function confirmHotelBooking(Request $request)
    {
        // return $request->all();
        $bookingHotel = new CustomerHotelBooking();
        $bookingHotel->select_package = $request->select_package;
        $bookingHotel->first_name = $request->first_name;
        $bookingHotel->last_name = $request->last_name;
        $bookingHotel->contact_num = $request->contact_num;
        $bookingHotel->booking_date = $request->booking_date;
        $bookingHotel->email = $request->email;
        $bookingHotel->password = $request->password;
        $bookingHotel->gender = $request->gender;
        $bookingHotel->nid = $request->nid;
        $bookingHotel->passport_no = $request->passport_no;
        $bookingHotel->save();

        return view('front.hotel.hotel-booking-list', [
            'hotel' => CustomerHotelBooking::find($bookingHotel->id)
        ]);
    }
}
