<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerHotelBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_hotel_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('select_package');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('contact_num');
            $table->string('booking_date');
            $table->string('email');
            $table->string('password');
            $table->string('gender');
            $table->string('nid');
            $table->string('passport_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_hotel_bookings');
    }
}
