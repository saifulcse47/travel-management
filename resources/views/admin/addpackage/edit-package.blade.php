@extends('admin.master')

@section('title')
    admin
@endsection

@section('body')
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <form style="align-items: center; position: center" action="{{ url('/update-package') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-md-6"> Package Name </label>
                        <div class="col-md-9">
                            <input type="hidden" name="package_id" value="{{ $package->id }}" class="form-control" >
                            <input type="text" name="package_name" value="{{ $package->package_name }}" class="form-control" >
                            <span style="color: red">{{ $errors->has('package_name') ? $errors->first('package_name') : ' ' }}</span>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6"> Package Description </label>
                        <div class="col-md-9">
                            <input type="text" name="package_description" value="{{ $package->package_description }}" class="form-control" >
                            <span style="color: red">{{ $errors->has('package_description') ? $errors->first('package_description') : ' ' }}</span>

                        </div>


                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6"> Package Price </label>
                        <div class="col-md-9">
                            <input type="number" name="package_price"   class="form-control" >
                            <span style="color: red">{{ $errors->has('package_price') ? $errors->first('package_price') : ' ' }}</span>

                        </div>

                    </div>



                    <div class="form-group">
                        <label class="control-label col-md-6"> Package Image </label>
                        <div class="col-md-9">
                            <input type="file" name="package_image" class="form-control" >
                            <span style="color: red">{{ $errors->has('package_image') ? $errors->first('package_image') : ' ' }}</span>

                        </div>

                    </div>
                    <div class="form-group">
                        <button style="color: green" type="submit" > Update</button>
                    </div>

                </form>


            </div>
        </div>
    </div>


@endsection


