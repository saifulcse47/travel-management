@extends('admin.master')

@section('title')
    View Package
@endsection

@section('body')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="margin-top: 50px">
                    <h1 class="text-success text-center">{{ Session::get('message') }}</h1>
                    <div class="panel panel-default">
                        <h3 class="text-center panel-heading">
                            Package
                        </h3>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Package Name</th>
                                    <th>Package Description</th>
                                    <th>Package Price</th>
                                    <th>Package Image</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i=1)
                                @foreach($packages as $package)
                                    <tr class="odd gradeX">
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $package->package_name }}</td>
                                        <td>{{ $package->package_description }}</td>
                                        <td>{{ $package->package_price }}</td>
                                        <td><img src="{{ asset($package->package_image) }}" alt="" style="height: 100px;width: 120px;" class="rounded img-fluid"></td>
                                        <td>
                                            <a href="{{ url('/delete-package/'.$package->id) }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                            <a href="{{ url('/edit-package/'.$package->id) }}">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </section>
@endsection