@extends('front.master')

@section('title')
    View Hotel List
@endsection

@section('body')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="margin-top: 50px">
                    <h1 class="text-success text-center">{{ Session::get('message') }}</h1>
                    <div class="panel panel-default">
                        <h3 class="text-center panel-heading">
                            Package
                        </h3>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th> First Name</th>
                                    <th>Last Name</th>
                                    <th>Contact Number</th>
                                    <th>Email Address</th>
                                    <th>Booking Date</th>
                                    <th>Gender</th>
                                    <th>Select Package</th>
                                    <th>nid</th>
                                    <th>Passport Number</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i=1)
                                </tbody>
                                    <tr class="odd gradeX">
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $hotel->first_name }}</td>
                                        <td>{{ $hotel->last_name }}</td>
                                        <td>{{ $hotel->contact_num }}</td>
                                        <td>{{ $hotel->email }}</td>
                                        <td>{{ $hotel->booking_date }}</td>
                                        <td>{{ $hotel->gender }}</td>
                                        <td>{{ $hotel->select_package }}</td>
                                        <td>{{ $hotel->nid }}</td>
                                        <td>{{ $hotel->passport_no }}</td>
                                        <td>Delete</td>
                                    </tr>
                                    </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </section>
@endsection