@extends('front.master')

@section('title')
    View Booking
@endsection

@section('body')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="margin-top: 50px">
                    <h1 class="text-success text-center">{{ Session::get('message') }}</h1>
                    <div class="panel panel-default">
                        <h3 class="text-center panel-heading">
                            Package
                        </h3>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th> First Name</th>
                                    <th>Last Name</th>
                                    <th>Contact Number</th>
                                    <th>Email Address</th>
                                    <th>Booking Date</th>
                                    <th>Gender</th>
                                    <th>Select Package</th>
                                    <th>nid</th>
                                    <th>Passport Number</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i=1)
                                </tbody>
                                @foreach($customerInfos as $package)
                                    <tr class="odd gradeX">
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $package->first_name }}</td>
                                        <td>{{ $package->last_name }}</td>
                                        <td>{{ $package->contact_num }}</td>
                                        <td>{{ $package->email }}</td>
                                        <td>{{ $package->booking_date }}</td>
                                        <td>{{ $package->gender }}</td>
                                        <td>{{ $package->select_package }}</td>
                                        <td>{{ $package->nid }}</td>
                                        <td>{{ $package->passport_no }}</td>
                                        <td>Delete</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </section>
@endsection